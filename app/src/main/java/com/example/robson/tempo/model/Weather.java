package com.example.robson.tempo.model;


public class Weather {

  public Integer _id;
  private String date;
  private String weekday;
  private String max;
  private String min;
  private String description;
  private String condition;
  private String city;

    public  Weather(){}

    public Weather(int _id, String date, String weekday, String max, String min, String description, String condition, String city) {

        this._id = _id;
        this.date = date;
        this.weekday = weekday;
        this.max = max;
        this.min = min;
        this.description = description;
        this.condition = condition;
        this.city = city;
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return  weekday+ " "+ date + "\n"+
                "Temperatura Max: " + max + "\n"+
                "Temperatura Min :" + min + "\n"+
                description +"\n"+
                city;
    }
}

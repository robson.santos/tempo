package com.example.robson.tempo.model;

public class WeatherToday {

   private Integer _id;
   private String temperatura;
   private String date;
   private String description;
   private String currently;
   private String city_name;

   public  WeatherToday(){}

    public WeatherToday(Integer _id, String temperatura, String date, String description, String currently, String city_name) {
        this._id = _id;
        this.temperatura = temperatura;
        this.date = date;
        this.description = description;
        this.currently = currently;
        this.city_name = city_name;
    }
    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public void settemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCurrently(String currently) {
        this.currently = currently;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String gettemperatura() {
        return temperatura;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getCurrently() {
        return currently;
    }

    public String getCity_name() {
        return city_name;
    }

    @Override
    public String toString() {
        return "\n" + getCity_name() + "\n" + "\n"+ getDescription() + "\n";
    }
}

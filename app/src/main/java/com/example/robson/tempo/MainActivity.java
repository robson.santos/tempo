package com.example.robson.tempo;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.ArrayRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.robson.tempo.dao.WeatherDAO;
import com.example.robson.tempo.dao.WeatherTodayDAO;
import com.example.robson.tempo.model.Weather;
import com.example.robson.tempo.model.WeatherToday;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.robson.tempo.Util.UrlApi.URL_API;

public class MainActivity extends AppCompatActivity {



    ListView listViewWeather;
    Button btn;

    public final String CITYNAME ="cidade";
    private WeatherTodayDAO weatherTodayDAO;
    private WeatherDAO weatherDAO;
    private Weather weather;
    private RequestQueue mQueue;
    private List<WeatherToday> listWeather;
    private String city;

    private AlertDialog alerta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewWeather = (ListView) findViewById(R.id.lista);
        mQueue = Volley.newRequestQueue(this);
        weatherTodayDAO = new WeatherTodayDAO(this);
        weatherDAO = new WeatherDAO(this);
        listWeather = weatherTodayDAO.listWeatherToday();
        btn = (Button) findViewById(R.id.btnTeste);

        Intent it = getIntent();
        city = it.getStringExtra("nomecidade");

        if(city!=null){
            JsonWeatherToday(city);
            JsonWeatherList(city);
        }

        GerarLista();


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CidadeActivity.class);
                startActivity(intent);
            }
        });


        listViewWeather.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Object o = listViewWeather.getItemAtPosition(position);
                WeatherToday weatherToday = (WeatherToday) listViewWeather.getItemAtPosition(position);
                String city = weatherToday.getCity_name();
                Intent intent = new Intent(MainActivity.this, WeatherDetail.class);
                intent.putExtra(CITYNAME, city);
                startActivity(intent);

            }
        });

    }



    private void GerarLista(){
        listWeather = weatherTodayDAO.listWeatherToday();
        if(listWeather!=null){
            ArrayAdapter<WeatherToday> dataApter = new ArrayAdapter<WeatherToday>(this,android.R.layout.simple_list_item_1,listWeather);
            listViewWeather.setAdapter(dataApter);
        }
    }


    public void JsonWeatherToday(String cidade){
        if(cidade.equals("Blumenau")){
            cidade = "455846 ";
        }
        if(cidade.equals("Itajai")){
            cidade = "455870  ";
        }
        if(cidade.equals("Timbó")){
            cidade = "458185 ";
        }
        if(cidade.equals("Balneario")){
            cidade = "423256 ";
        }
        String url = URL_API.concat(cidade);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response != null){
                                WeatherToday weatherToday = new WeatherToday();
                                JSONObject js = response.getJSONObject("results");
                                weatherToday.settemperatura(js.getString("temp"));
                                weatherToday.setDate(js.getString("date"));
                                weatherToday.setDescription(js.getString("description"));
                                weatherToday.setCurrently(js.getString("currently"));
                                weatherToday.setCity_name(js.getString("city_name"));
                                if(weatherToday !=null ){
                                    weatherTodayDAO.saveWeatherToday(weatherToday);
                                    GerarLista();
                                }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void JsonWeatherList(String cidade){
        if(cidade.equals("Blumenau")){
            cidade = "455846 ";
        }
        if(cidade.equals("Itajai")){
            cidade = "455870  ";
        }
        if(cidade.equals("Timbó")){
            cidade = "458185 ";
        }
        if(cidade.equals("Balneario")){
            cidade = "423256 ";
        }
        String url = URL_API.concat(cidade);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(weather==null){
                                weather = new Weather();
                            }
                            JSONObject js = response.getJSONObject("results");
                            weather.setCity(js.getString("city_name"));
                            JSONArray jsonArray = js.getJSONArray("forecast");
                            if(jsonArray!=null){
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject employee = jsonArray.getJSONObject(i);
                                    weather.setDate(employee.getString("date"));
                                    weather.setWeekday(employee.getString("weekday"));
                                    weather.setMax(employee.getString("max"));
                                    weather.setMin(employee.getString("min"));
                                    weather.setDescription(employee.getString("description"));
                                    weather.setCondition(employee.getString("condition"));
                                    if(weather!=null){
                                        weatherDAO.saveWeather(weather);
                                    }
                                 }
                             }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mQueue.add(request);

    }



}

package com.example.robson.tempo.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String BANCO_DADOS = "task";
    private static final int VERSAO = 1;

    public DataBaseHelper(Context context) {
        super(context, BANCO_DADOS, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Criar tabela DiaSemana
        db.execSQL("create table weathers (_id integer primary key autoincrement," +
                "date text, weekday text, max text, min text, description tex, condition text, city text)");


        //Criar tabela DiaAtual
        db.execSQL("create table weather_todays (_id integer primary key autoincrement," +
                "temperatura text, date text, description text, currently text, city_name tex)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static class Weathers {
        public static final String TABELA = "weathers";
        public static final String _ID = "_id";
        public static final String DATE = "date";
        public static final String WEEKDAY = "weekday";
        public static final String MAX = "max";
        public static final String MIN = "min";
        public static final String DESCRIPTION = "description";
        public static final String CONDITION = "condition";
        public static final String CITY = "city";


        public static  final String[] COLUNAS = new String[]{
                _ID,DATE,WEEKDAY,MAX,MIN,DESCRIPTION,CONDITION,CITY
        };

    }

    public static class WeatherTodays {
        public static final String TABELA = "weather_todays";
        public static final String _ID = "_id";
        public static final String TEMPERATURA = "temperatura";
        public static final String DATE = "date";
        public static final String DESCRIPTION = "description";
        public static final String CURRENTLY = "currently";
        public static final String CITY_NAME = "city_name";

        public static  final String[] COLUNAS = new String[]{
                _ID,TEMPERATURA,DATE,DESCRIPTION,CURRENTLY,CITY_NAME
        };

    }

}

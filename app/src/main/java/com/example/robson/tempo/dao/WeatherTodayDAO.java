package com.example.robson.tempo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.robson.tempo.model.WeatherToday;

import java.util.ArrayList;
import java.util.List;

public class WeatherTodayDAO {

    private DataBaseHelper databaseHelper;
    private SQLiteDatabase database;

    public WeatherTodayDAO(Context context){
        databaseHelper = new DataBaseHelper(context);
    }

    private SQLiteDatabase getDatabase(){
        if(database == null){
            database = databaseHelper.getWritableDatabase();
        }
        return database;
    }

    public WeatherToday createWeatherToday(Cursor cursor){
        WeatherToday model = new WeatherToday(
                cursor.getInt(cursor.getColumnIndex(DataBaseHelper.WeatherTodays._ID)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.WeatherTodays.TEMPERATURA)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.WeatherTodays.DATE)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.WeatherTodays.DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.WeatherTodays.CURRENTLY)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.WeatherTodays.CITY_NAME))
        );

        return  model;
    }

    public List<WeatherToday> listWeatherToday(){

        Cursor cursor = getDatabase().query(DataBaseHelper.WeatherTodays.TABELA,
                DataBaseHelper.WeatherTodays.COLUNAS,null,null,null,null,null);

        List<WeatherToday> weathersToday = new ArrayList<WeatherToday>();
        while(cursor.moveToNext()){
            WeatherToday model = createWeatherToday(cursor);
            weathersToday.add(model);
        }
        cursor.close();
        return weathersToday;
    }


    public long saveWeatherToday(WeatherToday weatherToday){
        ContentValues valores = new ContentValues();

        valores.put(DataBaseHelper.WeatherTodays.TEMPERATURA,weatherToday.gettemperatura());
        valores.put(DataBaseHelper.WeatherTodays.DATE, weatherToday.getDate());
        valores.put(DataBaseHelper.WeatherTodays.DESCRIPTION,weatherToday.getDescription());
        valores.put(DataBaseHelper.WeatherTodays.CURRENTLY,weatherToday.getCurrently());
        valores.put(DataBaseHelper.WeatherTodays.CITY_NAME,weatherToday.getCity_name());

        if(weatherToday.get_id()!=null){
            return  getDatabase().update(DataBaseHelper.WeatherTodays.TABELA, valores, "_id = ?", new String[]{weatherToday.get_id().toString()});
        }

        return getDatabase().insert(DataBaseHelper.WeatherTodays.TABELA, null,valores);
    }

    public boolean removeWeatherToday(int id){
        return getDatabase().delete(DataBaseHelper.WeatherTodays.TABELA, "_id = ?", new String[]{Integer.toString(id)}) > 0;
    }

    public WeatherToday searchWeatherId(int id){
        Cursor cursor = getDatabase().query(DataBaseHelper.WeatherTodays.TABELA, DataBaseHelper.WeatherTodays.COLUNAS, "_id = ?", new String[]{Integer.toString(id)},null,null,null);

        if(cursor.moveToNext()){
            WeatherToday model = createWeatherToday(cursor);
            cursor.close();
            return model;
        }
        return  null;
    }




}


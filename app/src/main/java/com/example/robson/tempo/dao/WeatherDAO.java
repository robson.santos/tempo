package com.example.robson.tempo.dao;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.robson.tempo.model.Weather;

import java.util.ArrayList;
import java.util.List;

public class WeatherDAO {

    private DataBaseHelper databaseHelper;
    private SQLiteDatabase database;

    public WeatherDAO(Context context){
        databaseHelper = new DataBaseHelper(context);
    }

    private SQLiteDatabase getDatabase(){
        if(database == null){
            database = databaseHelper.getWritableDatabase();
        }
        return database;
    }


    private Weather createWeather(Cursor cursor){
        Weather model = new Weather(
                cursor.getInt(cursor.getColumnIndex(DataBaseHelper.Weathers._ID)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Weathers.DATE)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Weathers.WEEKDAY)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Weathers.MAX)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Weathers.MIN)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Weathers.DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Weathers.CONDITION)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Weathers.CITY))
                );

        return  model;
    }

    public List<Weather> listWeather(){

        Cursor cursor = getDatabase().query(DataBaseHelper.Weathers.TABELA,
                DataBaseHelper.Weathers.COLUNAS,null,null,null,null,null);

        List<Weather> weathers = new ArrayList<Weather>();
        while(cursor.moveToNext()){
            Weather model = createWeather(cursor);
            weathers.add(model);
        }
        cursor.close();
        return weathers;
    }

    public List<Weather> listWeatherName(String city){

        Cursor cursor = getDatabase().query(DataBaseHelper.Weathers.TABELA, DataBaseHelper.Weathers.COLUNAS, "city = ?", new String[]{(city)},null,null,null);

        List<Weather> weathers = new ArrayList<Weather>();
        while(cursor.moveToNext()){
            Weather model = createWeather(cursor);
            weathers.add(model);
        }
        cursor.close();
        return weathers;
    }

    public long saveWeather(Weather weather){
        ContentValues valores = new ContentValues();

        valores.put(DataBaseHelper.Weathers.DATE,weather.getDate());
        valores.put(DataBaseHelper.Weathers.WEEKDAY,weather.getWeekday());
        valores.put(DataBaseHelper.Weathers.MAX,weather.getMax());
        valores.put(DataBaseHelper.Weathers.MIN,weather.getMin());
        valores.put(DataBaseHelper.Weathers.DESCRIPTION,weather.getDescription());
        valores.put(DataBaseHelper.Weathers.CONDITION,weather.getCondition());
        valores.put(DataBaseHelper.Weathers.CITY, weather.getCity());

        if(weather.get_id()!=null){
            return  getDatabase().update(DataBaseHelper.Weathers.TABELA, valores, "_id = ?", new String[]{weather.get_id().toString()});
        }

        return getDatabase().insert(DataBaseHelper.Weathers.TABELA, null,valores);
    }

    public boolean removeWeather(int id){
        return getDatabase().delete(DataBaseHelper.Weathers.TABELA, "_id = ?", new String[]{Integer.toString(id)}) > 0;
    }

    public Weather searchWeatherId(String city){
        Cursor cursor = getDatabase().query(DataBaseHelper.Weathers.TABELA, DataBaseHelper.Weathers.COLUNAS, "city = ?", new String[]{(city)},null,null,null);

        if(cursor.moveToNext()){
            Weather model = createWeather(cursor);
            cursor.close();
            return model;
        }
        return  null;
    }

}


